package com.example.hw13a;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.RemoteAction;
import android.app.RemoteInput;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final String BATTERY_LOW = "android.intent.action.BATTERY_LOW";
    private final String CHANNEL_ID = "CHANNEL_ID";
    private NotificationManager notificationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        createNotificationChannel();

        Button btnSendBroadcast = findViewById(R.id.sendBroadcast);
        Button btnSendBroadcastDynamic = findViewById(R.id.sendBroadcastDynamic);
        Button btnRegisterBroadcastDynamic = findViewById(R.id.registerBroadcast);
        Button btnStartService = findViewById(R.id.startService);
        Button btnSendNotification = findViewById(R.id.sendNotification);

        btnSendNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String replyLabel = "Enter Reply here";
                RemoteInput remoteInput =
                        new RemoteInput.Builder("KEY_TEXT_REPLY")
                                .setLabel(replyLabel)
                                .build();

                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        MainActivity.this,
                        100,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

                Notification.Action replyAction =
                        new Notification.Action.Builder(
                                R.drawable.ic_launcher_foreground,
                                "Reply",
                                pendingIntent
                        ).addRemoteInput(remoteInput).build();

                Notification notification = new Notification.Builder(MainActivity.this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentTitle("New Message")
                        .setContentText("Hello User")
                        .setAutoCancel(true)
                        .addAction(replyAction)
                        .build();

                notificationManager.notify(1, notification);
            }
        });

        btnStartService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MyService.class);
                startService(intent);
            }
        });

        btnRegisterBroadcastDynamic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerBroadcast();
            }
        });

        btnSendBroadcastDynamic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("com.example.ACTION");
                intent.putExtra("com.example.MESSAGE", "hello");
                sendBroadcast(intent);
            }
        });
    }

    private void registerBroadcast() {
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String s = intent.getStringExtra("com.example.MESSAGE");
                Log.d("Hello", s);
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
            }
        };
        IntentFilter intentFilter = new IntentFilter("com.example.ACTION");
        registerReceiver(broadcastReceiver, intentFilter);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Notification channel",
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationChannel.setDescription("My Notification channel");
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }
}