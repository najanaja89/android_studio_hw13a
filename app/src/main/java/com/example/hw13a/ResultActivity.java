package com.example.hw13a;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.RemoteInput;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        TextView resultMessage = findViewById(R.id.resultMessage);

        Intent intent = getIntent();
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);

        if (!remoteInput.isEmpty()) {
            String inputText = remoteInput.getCharSequence("KEY_TEXT_REPLY").toString();
            resultMessage.setText(inputText);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Notification notification =
                        new Notification.Builder(ResultActivity.this, "CHANNEL ID")
                                .setSmallIcon(R.drawable.ic_launcher_foreground)
                                .setContentText("Reply Received")
                                .build();

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(1, notification);
            }
        }
    }
}