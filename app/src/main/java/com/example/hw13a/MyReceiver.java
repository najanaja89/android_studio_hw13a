package com.example.hw13a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Log.d("Hello", "My Receiver");
        String s = intent.getStringExtra("EXTRA_MESSAGE");
        Toast.makeText(context, "onReceive", Toast.LENGTH_SHORT).show();
    }
}